//
//  FlagsFile.swift
//  VYOMO
//
//  Created by apple on 23/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import Foundation
import UIKit
//var responses = ViewController()
var jsonResult : NSDictionary = NSDictionary()
//var viewController = ViewController()

func get(url : String, viewController:ViewController) -> NSDictionary {
    var request : NSMutableURLRequest = NSMutableURLRequest()
    request.URL = NSURL(string: url)
    request.HTTPMethod = "GET"   
    
   var responses: AutoreleasingUnsafeMutablePointer<NSURLResponse?
    >=nil
    var error: NSErrorPointer = nil
    
    var dataVal: NSData =  NSURLConnection.sendSynchronousRequest(request, returningResponse: responses, error:nil)!
    var err: NSError
    println(responses)
    var json: NSDictionary = NSJSONSerialization.JSONObjectWithData(dataVal, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
    if (json != "<null>") {
        jsonResult = json
        
    }    
    return jsonResult
}

func post( url : String, postParams: String, viewController:ViewController) -> NSDictionary {
    var request : NSMutableURLRequest = NSMutableURLRequest()
    request.URL = NSURL(string: url)
    request.HTTPMethod = "POST"
    var bodyData = postParams
    request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
    
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let json: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            if (json != nil) {
                jsonResult = json
               // println(jsonResult)
            }
        })
    return jsonResult
}



    