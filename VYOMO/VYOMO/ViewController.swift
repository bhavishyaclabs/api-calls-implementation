//
//  ViewController.swift
//  VYOMO
//
//  Created by apple on 23/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
   
     override func viewDidAppear(animated: Bool) {
      
       super.viewDidAppear(animated)
        var urlpost : String = "http://54.173.217.54:8080/add_business"
        var urlget : String = "http://54.173.217.54:8080/get_all_business"
       
         var postParams : String = "business_name=BHAVIshya&business_address=value&business_country=India&business_state=Delhi&business_city=NewDelhi&business_zip_code=123456&business_phone_number_prefix=91&business_phone_number=0987654321&business_longitude=78.9089&business_latitude=90.7893&business_description=what?&business_type_id=909090&front_view_images=image.png&inside_view_images=im.png&side_view_imagesima.png"
        
        
            var result = get(urlget,self)
            var postResult = post(urlpost,postParams,self)
            responseStatus()
            println("result of get method in view did load = \(result)")
            println("result of post method in view did load = \(postResult)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


func responseStatus() {
    
    if let flag: Int = jsonResult["status"] as? Int {
        switch flag {
        case 200:
            println("Action Complete")
            //gives an alert message...
            let alert = UIAlertController(title: "Alert", message: "Action complete.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            
        case 100:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "PARAMETER_MISSING", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            
        case 201,404,400:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "ERROR", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
             self.presentViewController(alert, animated: true, completion: nil)
            
        case 204,101,102,103,104,105,106,107:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "INVALID", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        default:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "ERROR", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}
}

    
 //FOR MY REFRENCE...
   /* func getBuisnessType() {
        
        var url : String = "http://54.173.217.54:8080/get_business_type"
        var request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        request.HTTPMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            
            if (jsonResult != nil) {
                //For status..
                if let status: Int = jsonResult["status"] as? Int {
                    if status == 200 {
                        println(jsonResult["message"]!)
                        //For Data..
                        if let data: NSDictionary = jsonResult["data"] as? NSDictionary {
                            if let insideData: NSArray = data["business_type"] as? NSArray {
                                for i:Int in 0..<insideData.count {
                                    if let buisness: NSDictionary = insideData[i]  as? NSDictionary {
                                        if let buisnessId: Int = buisness["business_type_id"] as? Int {
                                            if let buisnessName: String = buisness["business_type_name"] as? String {
                                                println(buisnessName)
                                                println(buisnessId)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else if status == 201 {
                        println(jsonResult["message"])
                        if let data: NSDictionary = jsonResult["data"] as? NSDictionary {
                            if let insideData: NSArray = data["business_type"] as? NSArray {
                                println(insideData)
                            }
                        }
                    }
                    else if status == 404 {
                        println(jsonResult["message"])
                    }
                }
                
                    
                
            
            
            
            //For Message...
            if (jsonResult != nil) {
                if let Message: String = jsonResult["message"] as? String {
                    println(Message)
                }
            }else {
                println(error)
                
                }
            
        }
        })
        }*/
    
    
    // var postParams = ["business_name":"BHAVIshya", "business_address":"value", "business_country":"India", "business_state":"Delhi", "business_city":"NewDelhi", "business_zip_code":"123456", "business_phone_number_prefix":"91", "business_phone_number":"0987654321", "business_longitude":"78.9089", "business_latitude":"90.7893", "business_description":"what?" , "business_type_id":"909090", "front_view_images":"image.png", "inside_view_images":"im.png", "side_view_images":"ima.png"] as Dictionary<String, String>
    // Do any additional setup after loading the view, typically from a nib.
    //self.post(urlpost, postParams: postParams)
    


